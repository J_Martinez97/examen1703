<?php require 'views/header.php'; ?>
<main>
    <div>

        <h1>Lista de Usuarios</h1>

        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>nickName</th>
                <th>Contrasenya</th>
                <th>Provincia</th>
            </tr>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo $user->id ?></td>
                    <td><?php echo $user->name ?></td>
                    <td><?php echo $user->login ?></td>
                    <td><?php echo $user->password ?></td>
                    <?php $province_name = User::Province($user->password) ?>
                    <td><?php echo $province_name->name ?></td>
                </tr>
            <?php endforeach ?>
        </table>
        <p>
        <a href="/user/create">Nuevo</a>
        </p>
    </div>

</main>
<?php require 'views/footer.php'; ?>
