<?php require 'views/header.php'; ?>
<main>
    <div>

        <h1>ERROR <?php echo http_response_code() ?></h1>
        <h2>Excepción <?php echo $e->getCode() ?></h2>

        <h3><?php echo $e->getMessage() ?></h3>

        <pre>
            <?php
            var_dump($e->getTrace());
            ?>
        </pre>

    </div>

</main>
<?php require 'views/footer.php'; ?>
