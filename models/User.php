<?php

require_once('app/Model.php');
require_once('models/Province.php');

/**
*
*/
class User extends Model
{

    public $id;
    public $name;
    public $login;
    public $password;
    public $province_id;

    function __construct()
    {
        #
    }

    function all() {

        $db = User::connect();

        $sql = "SELECT * FROM users";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $results = $stmt->fetchAll();
        return $results;

    }


    public function store()
    {
        $db = $this->connect();

        $sql = "INSERT INTO users VALUES(:id, :name, :login, :password, :province_id)";
        $stmt = $db->prepare($sql);
        $stmt->bindparam(':id', $this->id);
        $stmt->bindparam(':name', $this->name);
        $stmt->bindparam(':login', $this->login);
        $stmt->bindparam(':password', $this->password);
        $stmt->bindparam(':province_id', $this->province_id);
        return $stmt->execute();
    }


    public function find($id)
    {
        $db = User::connect();
        $sql = "SELECT * FROM users WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $result = $stmt->fetch();
        // var_dump($result);
        // die();
        return $result;
    }


       public function findUser($password, $login)
    {

        $db = User::connect();
        $sql = "SELECT * FROM users WHERE ( password = :password && login = :login )";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':login', $login);
        $stmt->bindParam(':password', $password);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $result = $stmt->fetch();
        // var_dump($result);
        // die();
        return $result;
    }

       public function Province()
    {
            $province = Province::find($this->province_id);

        return $province;
    }
}
