<?php

/**
*
*/
require_once('app/Model.php');

class Province extends Model
{

    function __construct()
    {
        # code...
    }

    public function all()
    {
        $db = Type::connect();

        $sql = "SELECT * FROM provinces";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Type');
        $results = $stmt->fetchAll();
        return $results;
    }

     public function find($id)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM provinces WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Province');
        $result = $stmt->fetch();
        // var_dump($result);
        // die();
        return $result;
    }

}
