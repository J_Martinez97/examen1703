# Examen Desarrollo Web en Entorno Servidor DWES1703
## Recuperación PHP. Nota máxima 6 (100 puntos). Nota para recuperar (75 puntos).

 Antes de empezar:

    * Inicia la captura de pantallas cada 5 segundos. Usa chronolapse o el script que te facilita el profesor. El destino del mismo debe ser tu pendrive.

    *  Haz un fork del repositorio
 https://bitbucket.org/daw2rafa/examen1703

    * Clona desde tu máquina de trabajo el repositorio
  que acabas de crear en tu cuenta Bitbucket. Ubicalo
  en `/var/www/examen1703`

    * Configuración de apache. En el repositorio hay un directorio apache que te ayudará:

        * Copia el fichero examen3.conf en /etc/sites/available/
        * Ejecuta: sudo a2ensite examen3
        * Copia el fichero hosts en /etc
        * Reinicia apache

    * Puedes iniciar el examen. No olvides entregar tu USB con las capturas de pantalla.

## Enunciado.

En el examen hemos de hacer un CRUD sobre la tabla
_users_.

Deben usarse y modificarse si es preciso los ficheros
 `header.php` y `footer.php`

Después de cada ejercicio haz un commit.

1. COMMIT1: 25P * Listado de _users_. Ruta `/user/index`.
2. COMMIT2: 25P * Creación de nuevos registros. Rutas
`/user/create` y `/user/store`
3. COMMIT3: 25P. Uso de sesiones: login. Rutas `/login/index` y `/login/run`
    * `/login/index` formulario de login
    * `/login/run` procesar formulario (login+contraseña)
    * Si el usuario y contraseña se envía a pantalla inicial
    El usuario debe mostrarse en el header.
4. COMMIT4: 25P. Modifica la parte anterior para que:

 * En la lista de usuarios salga el nombre de provincia y no su id
 * En formulario de alta debe haber un select con la lista de las provincias en lugar de una caja de texto.

Al terminar sube tu código a Bitbucket y haz un pull
request al profesor.
